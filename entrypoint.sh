#!/bin/bash
sleep 2

#Install the Server
if [[ ! -d /home/container/server ]] || [[ ${SERVER_UPDATE} == "1" ]]; then
	if [[ -f /home/container/steam.txt ]]; then
		/home/container/steamcmd/steamcmd.sh +login ${STEAM_USER} +force_install_dir /home/container/server +app_update ${SERVER_APPID} validate +runscript /home/container/steam.txt
	else
		/home/container/steamcmd/steamcmd.sh +login ${STEAM_USER} +force_install_dir /home/container/server +app_update ${SERVER_APPID} validate +quit
	fi
fi

if [[ -f /home/container/preflight.sh ]]; then
	/home/container/preflight.sh
fi

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo "~/server: ${MODIFIED_STARTUP}"

cd /home/container/server

# $NSS_WRAPPER_PASSWD and $NSS_WRAPPER_GROUP have been set by the Dockerfile
export USER_ID=$(id -u)
export GROUP_ID=$(id -g)
envsubst < /passwd.template > ${NSS_WRAPPER_PASSWD}
export LD_PRELOAD=/libnss_wrapper.so

# Manage git repositories
BASEDIR=$(pwd)
REPOS=()
readarray REPOS <<< "$(find /home/container -type f -name .gitrepo)"
echo "Repo STR: ${REPOS[*]}"
if [ ${#REPOS[@]} -gt 1 ]; then
	mkdir -p tmp
	echo "Found ${#REPOS[@]} git repositories"
	for REPO in "${REPOS[@]}"
	do
		DIR=$(dirname "$REPO")
		REPO_DETAIL=()
		IFS=" " read -r -a REPO_DETAIL <<< "$(cat $REPO)"
		REPO_URL=${REPO_DETAIL[0]}
		cd "$DIR" || exit 1
		if [ ! -d .git ]; then
			echo "Git repo below $DIR not found. Cloning..."
			git clone --recursive "$REPO_URL" "$BASEDIR/tmp" || exit 1
			mv $BASEDIR/tmp/* $DIR/
			mv $BASEDIR/tmp/.* $DIR/
		fi 
		if [ -f .update_always ] || [ -f .update_once ]; then
			echo "Updating repository..."
			git pull
			if [ -e .update_once ]; then
				rm .update_once
			fi
		fi
	done
	if [ -e "$BASEDIR/tmp" ]; then
		rm -rf "$BASEDIR/tmp"
	fi
	cd "$BASEDIR" || exit 1
fi


# Run the Server
${MODIFIED_STARTUP}

if [ $? -ne 0 ]; then
    echo "PTDL_CONTAINER_ERR: There was an error while attempting to run the start command."
    exit 1
fi